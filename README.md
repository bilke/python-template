# Python template

## Customization

- Rename subfolder `my_project`
- Change all fields marked with `TODO` in `pyproject.toml`
- Fix `TODO`s in `tests/test_base.py`
- Add a license via GitLabs `Add LICENSE`-button on the project repository

## Development setup

Run the following in the terminal:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -e ".[test]"
```

## Testing

To run the unit tests:

```bash
pytest
```

## Linting

To run the linter:

```bash
ruff check .
```
